import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/') as Promise<any>;
  }

  getButtonSubmitText() {
    return element(by.css('button.submit')).getText() as Promise<string>;
  }

}
