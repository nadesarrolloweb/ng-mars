import { Component } from '@angular/core';
import { FormControl, FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { Grid } from './interfaces/grid'
import { Robot } from './interfaces/robot'
import { Scent } from './interfaces/scent'
import { Position } from './interfaces/position'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  form: FormGroup;
  isFormReady: boolean;

  config: any = {
    maxGridX: 50,
    maxGridY: 50,
    maxInstructionLength: 100
  }

  // grid model
  grid: Grid = {
    start: {
      x: 0,
      y: 0
    },
    end: {
      x: 0,
      y: 0
    },
    metadata: {
      x: [],
      y: []
    }
  };

  // array with maked robots
  robots: Array<Robot> = [];

  // array with scents coordinates
  scents: Array<Scent> = [];

  // array with output messages
  outputs: Array<string> = [];

  constructor(private formBuilder: FormBuilder) {
    this.buildForm();
  }


  /**
   * initialize form
   */
  buildForm() {
    this.form = this.formBuilder.group({
      gridX: new FormControl(null, [
        Validators.required,
        Validators.min(0),
        Validators.max(this.config.maxGridX)
      ]),
      gridY: new FormControl(null, [
        Validators.required,
        Validators.min(0),
        Validators.max(this.config.maxGridY)
      ]),
      robots: new FormArray([])
    });
  }


  /**
   * push formControl robot to robots array
   */
  addRobot() {
    const robots = this.form.controls.robots as FormArray;

    robots.push(this.formBuilder.group({
      position: new FormControl(null, Validators.required),
      instructions: new FormControl(null, [Validators.required, Validators.maxLength(this.config.maxInstructionLength)])
    }));
  }


  /**
   * excecute on form submit
   */
  excecute() {
    const form = this.form;
    const isFormValid = form.valid;

    // stop if isn't valid
    if(!isFormValid) {
      this.showError('Los datos no son validos');
      return;
    }

    // get robots array. stop if robots < 1
    const robots = form.value.robots;
    if(!robots.length) {
      this.showError('Agregar al menos un robot');
      return;
    }

    // make grid
    this.makeGrid(form);

    // make robots
    this.makeRobots(form);

    // excecute robots instructions
    this.excecuteRobotsInstructions();
  }


  /**
   * make grid array
   */
  private makeGrid(form: FormGroup) {

    // get grid limits
    const gridX = form.value.gridX;
    const gridY = form.value.gridY;

    // set grid start position (lower-left)
    this.grid.start.x = 0;
    this.grid.start.y = 0;

    // set grid end position (upper-right)
    this.grid.end.x = gridX + 1;
    this.grid.end.y = gridY + 1;

    // set grid metadata for work in template
    this.grid.metadata.x = this.makeNumbersArray(this.grid.end.x);
    this.grid.metadata.y = this.makeNumbersArray(this.grid.end.y);
  }


  /**
   * make robots array
   */
  makeRobots(form: FormGroup) {
    const robots = form.value.robots;
    this.robots = [];

    this.robots = robots.map(
      (robot: Robot) => {

        const position: string = robot.position;
        const instructions: string = robot.instructions;
        let positionArray: Array<any>;
        let instructionsArray: Array<string>;

        // split position input string
        positionArray = position.split(' ');
        robot.x = Number(positionArray[0]);
        robot.y = Number(positionArray[1]);
        robot.orientation = String(positionArray[2]);
        robot.instructionNumber = 0;

        // split instructions input string
        instructionsArray = instructions.split('');
        robot.instructionsArray = instructionsArray;

        return robot;
      }
    );
  }


  /**
   * excecute robots instructions in the grid
   */
  excecuteRobotsInstructions() {
    const robots: Array<Robot> = this.robots;

    robots.map(
      (robot: Robot, i: number) => {

        robot.instructionsArray.map(
          (instruction: string) => {
            robot.instructionNumber = i;

            switch(instruction) {
              case 'L':
              case 'R':
                robot.orientation = this.turnDirection(robot.orientation, instruction);
                break;

              case 'F':
                const newPosition: Position = this.goForward(robot.x, robot.y, robot.orientation);

                // check if left the grid
                if(newPosition.x == -1 || newPosition.y == -1 || newPosition.x == this.grid.end.x || newPosition.y == this.grid.end.y) {

                  if(!robot.isLost) {
                    this.makeScent({
                      x: robot.x,
                      y: robot.y,
                      orientation: robot.orientation
                    });
                  }

                  robot.isLost = true;
                }

                robot.x = newPosition.x;
                robot.y = newPosition.y;
                break;
            }

          }
        )

        const result = `${ robot.x } ${ robot.y } ${ robot.orientation } ${ robot.isLost ? 'LOST' : '' }`;
        this.outputs.push(result);
      }
    )

  }


  /**
   * turn robot direction
   * @param actualOrientation - actual cardinal point orientation
   * @param turnDirection - turn direction, left or right
   * @return newOrientation - new cardinal point orientation
   */
  private turnDirection (actualOrientation: string, turnDirection: string): string {
    const left = {
      "N": "W",
      "W": "S",
      "S": "E",
      "E": "N"
    };
    const right = {
      "N": "E",
      "E": "S",
      "S": "W",
      "W": "N"
    };
    let newOrientation: string;

    switch(turnDirection) {
      case "L":
        newOrientation = left[actualOrientation];
        break;

      case "R":
        newOrientation = right[actualOrientation];
        break;
    }

    return newOrientation;
  }


  /**
   * go forward
   * @param orientation - actual orientation cardinal point
   * @param x - actual x grid position
   * @param y - actual y grid position
   * @return {Position} new position
   */
  private goForward(x: number, y: number, orientation: string): Position {
    let newPosition: Position = {
      x,
      y
    };

    // check for scents and prevent left grid
    const hasScents = this.checkScentsByPosition(x, y, orientation)
    if(hasScents) {
      return newPosition;
    }

    switch(orientation) {
      case 'N':
        newPosition.y++;
        break;

      case 'E':
        newPosition.x++;
        break;

      case 'S':
        newPosition.y--;
        break;

      case 'W':
        newPosition.x--;
        break;
    }

    return newPosition;
  }


  /**
   * make grid scent
   */
  private makeScent(scent: Scent) {
    const exists: boolean = this.checkScentsByPosition(scent.x, scent.y, scent.orientation);

    if(!exists) {
      this.scents.push({
        x: scent.x,
        y: scent.y,
        orientation: scent.orientation
      })
    }
  }


  /**
   * get scent
   */
  private checkScentsByPosition(x: number, y: number, orientation: string) {
    const scents = this.scents;

    let exists = scents.find(
      (scent: Scent) => {
        if(scent.x == x && scent.y == y && scent.orientation == orientation) {
          return true;
        }
      }
    );

    if(exists) {
      return true;
    }
  }


  /**
   * return array from 0 to n
   * @param length - array length
   */
  private makeNumbersArray(length: number) {
    return Array.from({
      length: length
    }, (_, i) => i);
  }


  /**
   * show toast error
   * @param errorMessage - descriptive string
   */
  private showError(errorMessage: string) {
    alert(errorMessage);
  }

}
