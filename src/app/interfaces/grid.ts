export interface Grid {
  start: {
    x: number,
    y: number
  },
  end: {
    x: number,
    y: number
  },
  metadata: {
    x: Array<number>,
    y: Array<number>
  }
}
