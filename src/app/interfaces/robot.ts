export interface Robot {
  instructions: string,
  position: string,
  x?: number,
  y?: number,
  orientation?: string,
  isLost?: boolean,
  isOff?: boolean,
  instructionNumber?: number,
  instructionsArray?: Array<string>
}
