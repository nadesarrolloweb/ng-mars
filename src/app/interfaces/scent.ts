export interface Scent {
  x: number,
  y: number,
  orientation: string
}
